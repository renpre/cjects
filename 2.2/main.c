#include <stdio.h>

int main(int argc, char** argv)
{
	printf("   * |");
	for (int i = 1; i < 11; i++)
	{
		printf("%4d", i);
	}
	printf("\n");
	for (int i = 0; i < 11; i++)
	{
		for (int j = 0; j < 11; j++)
		{
			int x = i * j;
			if (x != 0)
			{
				printf("%4d", x);
			}
			else
			{
				if (i != 0)
				{
					printf("%4d |", i);
				}
				else
				{
					if (j != 0)
					{
						printf("----");
					}
					else
					{
						printf(" ----+");
					}
				}
			}
		}
		printf("\n");
	}
	system("pause");
	return 0;
}
#include <stdio.h>
#include <time.h>
#include <stdbool.h>

int main(int argc, char** argv)
{
	srand(time(NULL));
	while (true)
	{
		int iMaxRunden = 100000;
		int iGewonnen = 0;
		for (int iRunde = 0; iRunde < iMaxRunden; iRunde++)
		{
			int iApfel = 10;
			int iKirsche = 10;
			int iPflaume = 10;
			int iBirne = 10;
			int iRabe = 0;
			int iMaxSpieler = 4;
			int iAktuellerSpieler = 0;
			int iaKoerbchen[4][4] = { { 0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0} };
			bool BaumLeer = false;
			while (BaumLeer == false && iRabe < 9)
			{
				iAktuellerSpieler++;
				if (iAktuellerSpieler > 3)
				{
					iAktuellerSpieler = 0;
				}
				int iWurf = 0;
				iWurf = rand() % 6;
				switch (iWurf)
				{
				case 0://Gr�n
					if (iApfel > 0)
					{
						iApfel--;
						iaKoerbchen[iAktuellerSpieler][0]++;
					}
					break;
				case 1://Rot
					if (iKirsche > 0)
					{
						iKirsche--;
						iaKoerbchen[iAktuellerSpieler][1]++;
					}
					break;
				case 2://Blau
					if (iPflaume > 0)
					{
						iPflaume--;
						iaKoerbchen[iAktuellerSpieler][2]++;
					}
					break;
				case 3://Gelb
					if (iBirne > 0)
					{
						iBirne--;
						iaKoerbchen[iAktuellerSpieler][3]++;
					}
					break;
				case 4://K�rbchen
					for (int i = 0; i < 2; i++)
					{
						if (iApfel >= iBirne && iApfel >= iKirsche && iApfel >= iPflaume)
						{
							iApfel--;
							iaKoerbchen[iAktuellerSpieler][0]++;
						}
						else if (iKirsche >= iApfel && iKirsche >= iBirne && iKirsche >= iPflaume)
						{
							iKirsche--;
							iaKoerbchen[iAktuellerSpieler][1]++;
						}
						else if (iPflaume >= iBirne && iPflaume >= iKirsche && iPflaume >= iApfel)
						{
							iPflaume--;
							iaKoerbchen[iAktuellerSpieler][2]++;
						}
						else
						{
							iBirne--;
							iaKoerbchen[iAktuellerSpieler][3]++;
						}
					}
					break;
				case 5://Rabe
					if (iRabe < 9)
					{
						iRabe++;
					}
					break;
				}
				if (iApfel <= 0 && iKirsche <= 0 && iPflaume <= 0 && iBirne <= 0)
				{
					BaumLeer = true;
				}
			}
			if (BaumLeer == true)
			{
				//printf("Spieler hat Gewonnen\n");
				//printf("%d %d %d %d %d\n\n", iApfel, iKirsche, iPflaume, iBirne, iRabe);
				iGewonnen++;
			}
			else
			{
				//printf("Kranich hat Gewonnen\n");
				//printf("%d %d %d %d %d\n\n", iApfel, iKirsche, iPflaume, iBirne, iRabe);
			}
		}
		float fGewonnen = (float)iGewonnen;
		float fMaxRunden = (float)iMaxRunden;
		float fGewinnWahrscheinlichkeit = (fGewonnen / fMaxRunden) * 100.0;
		printf("Es wurden %.2f%% der Spiele Gewonnen\n", fGewinnWahrscheinlichkeit);
		system("PAUSE > NUL");
	}
	return 0;
}
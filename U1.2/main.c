#include <stdio.h>

int main()
{
	int iMax = 110;
	for (int i = 1; i <= iMax; i++)
	{
		int wahr = 0;
		if (i % 3 == 0)
		{
			printf("Fizz");
			wahr = 1;
		}
		if (i % 5 == 0)
		{
			printf("Buzz");
			wahr = 1;
		}
		if (i % 7 == 0)
		{
			printf("Foo");
			wahr = 1;
		}
		if (!wahr)
		{
			printf(" %d ", i);
		}
		printf("\n");
	}
	return 10;
}
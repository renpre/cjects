#include <stdio.h>

int is_leap_year(int year);
int main(int argc, char** argv)
{
	int iTag = 0;
	int iMonat = 0;
	int iJahr = 0;
	int iTage = 0;
	int daysInMonth[] = { 31,28,31,30,31,30,31,31,30,31,30,31 }; //Tage je Monat
	scanf_s("%d %d %d",&iTag,&iMonat,&iJahr); //Datum Einlesen
	if (iMonat > 2 && is_leap_year(iJahr))
		daysInMonth[1] = 27;
	for (int i = 0; i < iMonat-1; i++)
	{
		iTage += daysInMonth[i];
	}
	iTage += iTag;
	printf("\n\n%d Tage",iTage);
	return 0;
}

int is_leap_year(int year)
{
	if (year % 400 == 0)
	{
		return 1;
	}
	else if (year % 100 == 0)
	{
		return 0;
	}
	else if (year % 4 == 0)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}